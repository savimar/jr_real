<%@page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="ui.MainServlet"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Schedule</h1>
	<hr>
	<div>
		<h3>Enter the data for the formation of the schedule in the
			format 2015-09-01T09:30:00 separated by semicolons</h3>
	</div>
	<hr>
	<div>
		<form id="datetime-local" name="datetime-local" action="MainServlet"
			method="POST">
			<label for="datetime-local"><strong>Date and time</strong>:</label>
			<fieldset>
				<textarea name="datetime-local" rows="10" id="datetime-local">2015-09-01T09:30:00;
    </textarea>
			</fieldset>
			<hr>
			<input name="submit" type="submit" id="submit" title="Introduce list"
				value="Introduce list">
		</form>
	</div>
	<h3>${message}</h3>
	<div></div>
	<hr>
</body>
</html>