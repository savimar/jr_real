package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import domain.DomainException;
import domain.Teacher;

public class TeacherDao extends DataBaseDao {

	static Logger logger = Logger.getLogger(TeacherDao.class);

	public TeacherDao() throws DAOException {
		super();
	}

	public Teacher findByName(String name) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM teacher WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Teacher teacher = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameTeacher = resultSet.getString("name");
				String chair = resultSet.getString("chair");
				int numberHours = resultSet.getInt("number_hours");
				teacher = new Teacher(nameTeacher, numberHours, chair);
				logger.info("teacher " + name + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("teacher " + name + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return teacher;

	}

	public Teacher findById(int id) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM teacher WHERE id = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Teacher teacher = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameTeacher = resultSet.getString("name");
				String chair = resultSet.getString("chair");
				int numberHours = resultSet.getInt("number_hours");
				teacher = new Teacher(nameTeacher, numberHours, chair);
				logger.info("teacher id " + id + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("teacher id " + id + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return teacher;

	}

	public Teacher createTeacher(String name, int numberHours, String chair)
			throws DAOException, NamingException {
		String sql = "INSERT INTO teacher (name,chair,number_hours) VALUES (?, ?, ?)";
		Connection connection = null;
		PreparedStatement statement = null;
		Teacher teacher = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			statement.setString(2, chair);
			statement.setInt(3, numberHours);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("New teacher " + name + " create");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return teacher;
	}

	public void deleteTeacher(String name, String chair) throws DAOException, NamingException {

		String sql = "DELETE FROM teacher WHERE name = ? AND chair = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			statement.setString(2, chair);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Teacher " + name + " delete");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}

	public List<Teacher> findTeachersGroup(int idSubject) throws DAOException,
			DomainException, NamingException {
		String sql = "SELECT name, chair, number_hours  FROM teacher  WHERE subject = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Teacher> teachers = new ArrayList<Teacher>();
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setInt(1, idSubject);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				logger.info("result set from query is");
				String nameTeacher = resultSet.getString("name");
				String chairDB = resultSet.getString("chair");
				int numberHours = resultSet.getInt("number_hours");
				teachers.add(new Teacher(nameTeacher, numberHours, chairDB));
				logger.info("Teacher " + nameTeacher + " chair " + chairDB
						+ " add to list group for subject");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return teachers;

	}

	public void updateTeacher(String name, String chair, String newName,
			String newChair, int newNumberHours) throws DAOException, NamingException {
		String sql = "UPDATE teacher set name = ?, number_hours = ?, chair = ? where name = ? AND  chair = ?";
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, newName);
			statement.setInt(2, newNumberHours);
			statement.setString(3, newChair);
			statement.setString(4, name);
			statement.setString(5, chair);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Teacher " + newName + " update");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}

}
