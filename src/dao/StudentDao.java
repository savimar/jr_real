package dao;

/**
 *  DAO student
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import domain.DomainException;
import domain.Student;

public class StudentDao extends DataBaseDao {

	static Logger logger = Logger.getLogger(StudentDao.class);

	public StudentDao() throws DAOException {
		super();
	}

	public Student findByName(String name) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM student WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Student student = null;
		try
		// (Connection connection = getConnection();
		// PreparedStatement statement = connection.prepareStatement(sql)){
		{
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameStudent = resultSet.getString("name");
				String idStudentDB = resultSet.getString("id_student");
				String specialization = resultSet.getString("specialization");
				student = new Student(nameStudent, idStudentDB, specialization);
				logger.info("student " + name + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("student " + name + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return student;

	}

	public Student findById(int id) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM student WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Student student = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameStudent = resultSet.getString("name");
				String idStudentDB = resultSet.getString("id_student");
				String specialization = resultSet.getString("specialization");
				student = new Student(nameStudent, idStudentDB, specialization);
				logger.info("student id " + id + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("student id " + id + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return student;

	}

	public Student createStudent(String name, String idStudent,
			String specialization) throws DAOException, NamingException {
		String sql = "INSERT INTO STUDENT (name,id_student,specialization) VALUES (?, ?, ?)";
		Connection connection = null;
		PreparedStatement statement = null;
		Student student = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			statement.setString(2, idStudent);
			statement.setString(3, specialization);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("New student " + name + " create");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return student;
	}

	public void deleteStudent(String name, String idStudent)
			throws DAOException, NamingException {

		String sql = "DELETE FROM student WHERE name = ? AND id_student = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			statement.setString(2, idStudent);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Student " + name + " delete");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}

	public List<Student> findStudentsGroup(String groupName)
			throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM student WHERE id_student LIKE ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Student> students = new ArrayList<Student>();
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, groupName);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				logger.info("result set from query is");
				String nameStudent = resultSet.getString("name");
				String idStudentDB = resultSet.getString("id_student");
				String specialization = resultSet.getString("specialization");
				students.add(new Student(nameStudent, idStudentDB,
						specialization));
				logger.info("Student " + nameStudent + " idSudent "
						+ idStudentDB + " add to list group "
						+ groupName.substring(0, groupName.length() - 1));
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return students;

	}

	public void updateStudent(String name, String idStudent, String newName,
			String newIdStudent, String newSpecialization) throws DAOException, NamingException {
		String sql = "UPDATE student set name = ?, id_student = ?, specialization = ? where name = ? AND  id_student = ?";
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, newName);
			statement.setString(2, newIdStudent);
			statement.setString(3, newSpecialization);
			statement.setString(4, name);
			statement.setString(5, idStudent);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Student " + newName + " update");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}

}