package dao;

/**
 * DAO parent
 */
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository("dataBaseDao")
public abstract class DataBaseDao {
	static Logger logger = Logger.getLogger(DataBaseDao.class);
	/*
	 * @PersistenceContext protected SessionFactory sessionFactory;
	 */

	private long id;

	public DataBaseDao() {

	}

	/*
	 * public SessionFactory getSessionFactory() { return sessionFactory; }
	 * 
	 * @Resource(name = "sessionFactory") public void
	 * setSessionFactory(SessionFactory sessionFactory) { this.sessionFactory =
	 * sessionFactory; }
	 */

	/*
	 * private InitialContext ic; private DataSource ds;
	 */

	DataSource ds;

	public Connection getConnection() throws SQLException, NamingException {
		Connection connection = null;

		try {
			if (this.ds == null) {
				ds = getDataSourseFronJNDI();
			}
			logger.info("Connection successfully");
			connection = ds.getConnection();
		} catch (Exception e) {
			logger.error("No connection", e);
		}
		return connection;
	}

	/*
	 * 
	 * private DataSource getDataSourseFronJNDI() throws NamingException {
	 * 
	 * ic = new InitialContext(); Context ec = (Context)
	 * ic.lookup("java:/comp/env"); return (DataSource)
	 * ec.lookup("/jdbc/univer");
	 * 
	 * // DataSource dataSource = null;
	 * 
	 * // JndiTemplate jndi = new JndiTemplate(); // try { /* PoolProperties p =
	 * new PoolProperties();
	 * p.setUrl("jdbc:postgresql://localhost:5433/univer");
	 * p.setDriverClassName("org.postgresql.Driver"); p.setUsername("postrges");
	 * p.setPassword("1302"); p.setJmxEnabled(true); p.setTestWhileIdle(false);
	 * p.setTestOnBorrow(true); p.setValidationQuery("SELECT 1");
	 * p.setTestOnReturn(false); p.setValidationInterval(30000);
	 * p.setTimeBetweenEvictionRunsMillis(30000); p.setMaxActive(100);
	 * p.setInitialSize(10); p.setMaxWait(10000);
	 * p.setRemoveAbandonedTimeout(60); p.setMinEvictableIdleTimeMillis(30000);
	 * p.setMinIdle(10); p.setLogAbandoned(true); p.setRemoveAbandoned(true);
	 * p.setJdbcInterceptors
	 * ("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
	 * "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
	 * dataSource.setPoolProperties(p);
	 */

	// dataSource = (DataSource) jndi.lookup("java:comp/env/jdbc/univer");

	/*
	 * } catch (NamingException e) {
	 * logger.error("NamingException for java:comp/env/jdbc/univer", e); }
	 * return dataSource;
	 */
	/*
	 * }
	 * 
	 * public DataSource getDs() { return ds; }
	 * 
	 * public void setDs(DataSource ds) { this.ds = ds; }
	 * 
	 * public DataBaseDao() throws DAOException { factory =
	 * HibernateUtil.getSessionFactory(); }
	 * 
	 * /** find id table from children by their name
	 * 
	 * @param nameDataBase
	 * 
	 * @param nameRecord
	 * 
	 * @return
	 * 
	 * @throws DAOException
	 * 
	 * @throws NamingException
	 */

	private DataSource getDataSourseFronJNDI() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * public long findIdforName(String nameDataBase, String nameRecord) throws
	 * DAOException, NamingException { List<DataBaseDao> list =
	 * sessionFactory.getCurrentSession().createQuery("FROM " + nameDataBase +
	 * " WHERE name =:name ").setParameter("name", nameRecord).list();
	 * 
	 * if (list.size() > 0) { logger.info("result set from query is");
	 * list.get(0).getId(); } else { logger.warn("Result set is empty");
	 * logger.warn("Record's id for " + nameRecord + " from table " +
	 * nameDataBase + " don't found!"); }
	 * 
	 * /* String sql = "SELECT * FROM " + nameDataBase + "  WHERE name = ?";
	 * Connection connection = null; PreparedStatement statement = null;
	 * ResultSet resultSet = null; int id = -1; try { connection =
	 * getConnection(); logger.info("Connection open"); statement =
	 * connection.prepareStatement(sql); logger.info("Statement create");
	 * statement.setString(1, nameRecord); resultSet = statement.executeQuery();
	 * 
	 * if (resultSet.next()) { logger.info("result set from query is"); id =
	 * resultSet.getInt("id"); logger.info("Record's id for " + nameRecord +
	 * " from table " + nameDataBase + " is found"); } else {
	 * logger.warn("Result set is empty"); logger.warn("Record's id for " +
	 * nameRecord + " from table " + nameDataBase + " don't found!"); }
	 * 
	 * } catch (SQLException e) { logger.error("Error connecting to database",
	 * e); throw new DAOException("Error connecting to database", e); } finally
	 * { try { if (resultSet != null) { resultSet.close(); }
	 * logger.info("Result set close");
	 * 
	 * if (statement != null) { statement.close(); }
	 * logger.info("Statement close");
	 * 
	 * if (connection != null) { connection.close(); }
	 * logger.info("Connection close");
	 * 
	 * }
	 * 
	 * catch (SQLException e) {
	 * logger.error("Result set or statement or connection don't close", e);
	 * throw new
	 * DAOException("Result set or statement or connection don't close", e);
	 * 
	 * } }
	 * 
	 * return id;
	 * 
	 * }
	 */

	public long getId() {
		return id;
	}

}
