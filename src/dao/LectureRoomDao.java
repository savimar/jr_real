package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import domain.DomainException;
import domain.LectureRoom;

public class LectureRoomDao extends DataBaseDao {

	static Logger logger = Logger.getLogger(LectureRoomDao.class);

	public LectureRoomDao() throws DAOException {
		super();
	}

	public LectureRoom findByName(String name) throws DAOException,
			DomainException, NamingException {
		String sql = "SELECT * FROM lecture_room WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		LectureRoom lectureRoom = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameLR = resultSet.getString("name");
				String type = resultSet.getString("type");
				int spaciousness = resultSet.getInt("spaciousness");
				lectureRoom = new LectureRoom(nameLR, spaciousness, type);
				logger.info("lecture room " + name + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("lecture room " + name + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return lectureRoom;

	}

	public LectureRoom findById(int id) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM lecture_room WHERE id = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		LectureRoom lectureRoom = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameLR = resultSet.getString("name");
				String type = resultSet.getString("type");
				int spaciousness = resultSet.getInt("spaciousness");
				lectureRoom = new LectureRoom(nameLR, spaciousness, type);
				logger.info("lecture room  id" + id + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("lecture room  id" + id + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return lectureRoom;

	}

	public LectureRoom createLectureRoom(String name, int spaciousness,
			String type) throws DAOException, NamingException {
		String sql = "INSERT INTO lecture_room (name,type,spaciousness) VALUES (?, ?, ?)";
		Connection connection = null;
		PreparedStatement statement = null;
		LectureRoom lectureRoom = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			statement.setString(2, type);
			statement.setInt(3, spaciousness);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("New lecture room " + name + " create");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return lectureRoom;
	}

	public void deleteLectureRoom(String name) throws DAOException, NamingException {

		String sql = "DELETE FROM lecture_room WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Lecture room " + name + " delete");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}

	public List<LectureRoom> findLectureRooms() throws DAOException,
			DomainException, NamingException {
		String sql = "SELECT *  FROM lecture_room";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<LectureRoom> lectureRooms = new ArrayList<LectureRoom>();
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				logger.info("result set from query is");
				String name = resultSet.getString("name");
				String type = resultSet.getString("type");
				int spaciousness = resultSet.getInt("spaciousness");
				lectureRooms.add(new LectureRoom(name, spaciousness, type));
				logger.info("LectureRoom " + name + " add to list");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return lectureRooms;

	}

	public void updateLectureRoom(String name, String newName,
			int newSpaciousness, String newType) throws DAOException, NamingException {
		String sql = "UPDATE lecture_room set name = ?, type = ?, spaciousness = ? where name = ?";
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, newName);
			statement.setInt(2, newSpaciousness);
			statement.setString(3, newType);
			statement.setString(4, name);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Teacher " + newName + " update");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}
}
