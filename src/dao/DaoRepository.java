package dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface DaoRepository {

	List<DataBaseDao> findAll();

	DataBaseDao getById(Integer id);

	void update(DataBaseDao dao);

	void delete(Integer id);

	void create(DataBaseDao dao);

}
