package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import domain.DomainException;
import domain.Group;
import domain.Student;
import domain.Subject;

public class GroupDao extends DataBaseDao {

	static Logger logger = Logger.getLogger(GroupDao.class);

	public GroupDao() throws DAOException {
		super();
	}

	public Group findByName(String name) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM group_student WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Group group = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameGroup = resultSet.getString("name");
				int id = resultSet.getInt("id");
				Student student = new Student();
				List<Student> students = student.findStudentsGroup(nameGroup);
				SubjectDao subjectBase = new SubjectDao();
				List<Subject> subjects = subjectBase.findSubjectGroup(id);
				group = new Group(nameGroup, students, subjects);
				logger.info("Group " + name + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("Group " + name + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return group;

	}

	public Group createGroup(String name) throws DAOException, NamingException {
		String sql = "INSERT INTO group_student (name) VALUES (?)";
		Connection connection = null;
		PreparedStatement statement = null;
		Group group = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("New group " + name + " create");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return group;
	}

	public void deleteGroup(String name) throws DAOException, NamingException {

		String sql = "DELETE FROM group_student WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Group " + name + " delete");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}

	public List<Group> findListGroup() throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM group_student";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Group> groups = new ArrayList<Group>();
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				logger.info("result set from query is");
				String name = resultSet.getString("name");

				groups.add(new Group(name));
				logger.info("Subject " + name + " add to list group");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return groups;

	}

	public void updateGroup(String name, String newName) throws DAOException, NamingException {
		String sql = "UPDATE group_student set name = ? where name = ?";
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, newName);
			statement.setString(2, name);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Subject " + newName + " update");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}

	public Group findByID(int id) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM group_student WHERE id = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Group group = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameGroup = resultSet.getString("name");
				Student student = new Student();
				List<Student> students = student.findStudentsGroup(nameGroup);
				SubjectDao subjectBase = new SubjectDao();
				List<Subject> subjects = subjectBase.findSubjectGroup(id);
				group = new Group(nameGroup, students, subjects);
				logger.info("Group " + nameGroup + " id = " + id + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("Group by " + id + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return group;

	}
}
