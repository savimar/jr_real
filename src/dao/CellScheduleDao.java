package dao;

/**
 * DAO class cell schedule
 */
import interfaces.ICellScheduleDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import domain.CellSchedule;
import domain.DomainException;
import domain.Group;
import domain.LectureRoom;
import domain.Subject;
import domain.Teacher;

@Transactional
@Repository("cellSSchedule")
public class CellScheduleDao extends DataBaseDao implements ICellScheduleDao {
	static Logger logger = Logger.getLogger(CellScheduleDao.class);

	// @Resource(name = "sessionFactory")
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public CellScheduleDao() throws DAOException {
		super();
	}

	/**
	 * create cell schedule to Database
	 * 
	 * @param group
	 * @param teacher
	 * @param lectureRoom
	 * @param subject
	 * @param dateTimeStart
	 * @return
	 * @throws DAOException
	 * @throws NamingException
	 */
	public CellSchedule createCellSchedule(Group group, Teacher teacher, LectureRoom lectureRoom, Subject subject, LocalDateTime dateTimeStart) throws DAOException, NamingException {

		String sql = "INSERT INTO cell_schedule (group_id, teacher_id, lecture_room_id, subject_id, data_time_start) VALUES (?, ?, ?, ?, ?)";
		Connection connection = null;
		PreparedStatement statement = null;
		CellSchedule cellSchedule = null;

		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			// statement.setInt(1, findIdforName("group_student",
			// group.getName()));
			// statement.setInt(2, findIdforName("teacher", teacher.getName()));
			// statement.setInt(3, findIdforName("lecture_room",
			// lectureRoom.getName()));
			// statement.setInt(4, findIdforName("subject", subject.getName()));
			statement.setTimestamp(5, Timestamp.valueOf(dateTimeStart));

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("New cell schedule for group" + group.getName() + " at " + dateTimeStart + " create");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error("Result set or statement or connection don't close", e);
				throw new DAOException("Result set or statement or connection don't close", e);

			}
		}
		return cellSchedule;
	}

	/**
	 * delete cell Schedule from database
	 * 
	 * @param group
	 * @param dateTimeStart
	 * @throws DAOException
	 * @throws NamingException
	 */
	public void deleteCellSchedule(Group group, LocalDateTime dateTimeStart) throws DAOException, NamingException {

		String sql = "DELETE FROM cell_schedule WHERE group_id = ? AND data_time_start = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			// statement.setInt(1, findIdforName("group_student",
			// group.getName()));
			statement.setTimestamp(2, Timestamp.valueOf(dateTimeStart));

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Cell schedule for group" + group.getName() + " at " + dateTimeStart + " delete");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error("Result set or statement or connection don't close", e);
				throw new DAOException("Result set or statement or connection don't close", e);

			}
		}
	}

	/**
	 * find all record cell schedule
	 * 
	 * @return
	 * @throws DAOException
	 * @throws DomainException
	 * @throws NamingException
	 */

	@Override
	@Transactional
	public List<CellSchedule> findAll() {

		List<CellSchedule> cellSchedules = sessionFactory.getCurrentSession().createQuery("FROM cell_schedule").list();
		logger.info("list all cell schedules ready");
		if (cellSchedules.size() <= 0) {
			logger.error("Result set of all list  cellschedules is empty");
		}

		return cellSchedules;

	}

	/**
	 * find cell schedule by date ant time start
	 * 
	 * @param dateTimeStart
	 * @return
	 * @throws DAOException
	 * @throws DomainException
	 * @throws NamingException
	 */

	public List<CellSchedule> findListCellScheduleByDate(LocalDateTime dateTimeStart) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM cell_schedule WHERE data_time_start = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<CellSchedule> cellSchedules = new ArrayList<CellSchedule>();
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setTimestamp(1, Timestamp.valueOf(dateTimeStart));
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				logger.info("result set from query is");
				GroupDao groupBase = new GroupDao();
				Group group = groupBase.findByID(resultSet.getInt("group_id"));
				TeacherDao teacherBase = new TeacherDao();
				Teacher teacher = teacherBase.findById(resultSet.getInt("teacher_id"));
				SubjectDao subjectBase = new SubjectDao();
				Subject subject = subjectBase.findById(resultSet.getInt("subject_id"));
				LectureRoomDao lectureRoomBase = new LectureRoomDao();
				LectureRoom lectureRoom = lectureRoomBase.findById(resultSet.getInt("lecture_room_id"));

				cellSchedules.add(new CellSchedule(group, teacher, lectureRoom, subject, dateTimeStart));
				logger.info("Cell schedule for group" + group.getName() + " at " + dateTimeStart + " add to list cell schedules");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error("Result set or statement or connection don't close", e);
				throw new DAOException("Result set or statement or connection don't close", e);

			}
		}
		return cellSchedules;

	}

	/**
	 * update cell schedule from Database
	 * 
	 * @param group
	 * @param dateTimeStart
	 * @param newGroup
	 * @param newTeacher
	 * @param newLectureRoom
	 * @param newSubject
	 * @param newDateTimeStart
	 * @throws DAOException
	 * @throws NamingException
	 */
	public void updateCellSchedule(Group group, LocalDateTime dateTimeStart, String newGroup, String newTeacher, String newLectureRoom, String newSubject, LocalDateTime newDateTimeStart) throws DAOException, NamingException {
		String sql = "UPDATE cell_schedule set group_id = ?, teacher_id = ?, lecture_room_id = ?. subject_id = ?, data_time_start = ? where group_id = ? AND data_time_start = ?";
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			// statement.setInt(1, findIdforName("group_student", newGroup));
			// statement.setInt(2, findIdforName("teacher", newTeacher));
			// statement.setInt(3, findIdforName("lecture_room",
			// newLectureRoom));
			// statement.setInt(4, findIdforName("subject", newSubject));
			// statement.setTimestamp(5, Timestamp.valueOf(newDateTimeStart));
			// statement.setInt(6, findIdforName("group_student",
			// group.getName()));
			statement.setTimestamp(7, Timestamp.valueOf(dateTimeStart));

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Cell schedule for group" + group.getName() + " at " + dateTimeStart + " update");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error("Result set or statement or connection don't close", e);
				throw new DAOException("Result set or statement or connection don't close", e);

			}
		}

	}

	/**
	 * find cell schedule by group and date and time start
	 * 
	 * @param group
	 * @param dateTimeStart
	 * @return
	 * @throws DAOException
	 * @throws DomainException
	 * @throws NamingException
	 */

	public CellSchedule findCellSchedule(Group group, LocalDateTime dateTimeStart) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM cell_schedule WHERE data_time_start = ? AND group_id = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		CellSchedule cellSchedule = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setTimestamp(1, Timestamp.valueOf(dateTimeStart));
			// statement.setInt(2, findIdforName("group_student",
			// group.getName()));

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				logger.info("result set from query is");
				TeacherDao teacherBase = new TeacherDao();
				Teacher teacher = teacherBase.findById(resultSet.getInt("teacher_id"));
				SubjectDao subjectBase = new SubjectDao();
				Subject subject = subjectBase.findById(resultSet.getInt("subject_id"));
				LectureRoomDao lectureRoomBase = new LectureRoomDao();
				LectureRoom lectureRoom = lectureRoomBase.findById(resultSet.getInt("lecture_room_id"));

				cellSchedule = new CellSchedule(group, teacher, lectureRoom, subject, dateTimeStart);
				logger.info("Cell schedule for group" + group.getName() + " at " + dateTimeStart + " add to list cell schedules");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error("Result set or statement or connection don't close", e);
				throw new DAOException("Result set or statement or connection don't close", e);

			}
		}
		return cellSchedule;

	}

	/*
	 * @Override public List<CellSchedule> findListByDate(LocalDateTime
	 * dateTime) { // TODO Auto-generated method stub return null; }
	 * 
	 * @Override public CellSchedule findById(Long id) { // TODO Auto-generated
	 * method stub return null; }
	 * 
	 * @Override public CellSchedule findBtGrooupAndDate(Group group,
	 * LocalDateTime dateTime) { // TODO Auto-generated method stub return null;
	 * }
	 * 
	 * @Override public CellSchedule findByName(String name) { // TODO
	 * Auto-generated method stub return null; }
	 */
	@Override
	public CellSchedule create(CellSchedule cellSchedule) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(CellSchedule dao) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(CellSchedule cellSchedule, CellSchedule newCellSchedule) {
		// TODO Auto-generated method stub

	}

}
