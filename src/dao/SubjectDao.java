package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import domain.DomainException;
import domain.Subject;
import domain.Teacher;

public class SubjectDao extends DataBaseDao {

	static Logger logger = Logger.getLogger(SubjectDao.class);

	public SubjectDao() throws DAOException {
		super();
	}

	public Subject findByName(String name) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM subject WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Subject subject = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameSubject = resultSet.getString("name");
				String typeLectureRoom = resultSet
						.getString("type_lecture_room");
				int id = resultSet.getInt("id");
				TeacherDao teacherBase = new TeacherDao();
				List<Teacher> teachers = teacherBase.findTeachersGroup(id);
				subject = new Subject(nameSubject, teachers, typeLectureRoom);
				logger.info("Subject " + name + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("Subject " + name + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return subject;

	}

	public Subject findById(int id) throws DAOException, DomainException, NamingException {
		String sql = "SELECT * FROM subject WHERE id = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		Subject subject = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			if (resultSet.next()) {
				logger.info("result set from query is");
				String nameSubject = resultSet.getString("name");
				String typeLectureRoom = resultSet
						.getString("type_lecture_room");
				TeacherDao teacherBase = new TeacherDao();
				List<Teacher> teachers = teacherBase.findTeachersGroup(id);
				subject = new Subject(nameSubject, teachers, typeLectureRoom);
				logger.info("Subject id " + id + " is found");
			} else {
				logger.warn("Result set is empty");
				logger.warn("Subject id " + id + " don't found!");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return subject;

	}

	public Subject createSubject(String name, String typeLectureRoom)
			throws DAOException, NamingException {
		String sql = "INSERT INTO subject (name,type_lecture_room) VALUES (?, ?)";
		Connection connection = null;
		PreparedStatement statement = null;
		Subject subject = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);
			statement.setString(2, typeLectureRoom);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("New subject " + name + " create");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return subject;
	}

	public void deleteSubject(String name) throws DAOException, NamingException {

		String sql = "DELETE FROM subject WHERE name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, name);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Subject " + name + " delete");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}

	public List<Subject> findSubjectGroup(int idGroup) throws DAOException,
			DomainException, NamingException {
		String sql = "SELECT *  FROM subject WHERE group_id = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		List<Subject> subjects = new ArrayList<Subject>();
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setInt(1, idGroup);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				logger.info("result set from query is");
				String name = resultSet.getString("name");
				String typeLectureRoom = resultSet
						.getString("type_lecture_room");

				subjects.add(new Subject(name, typeLectureRoom));
				logger.info("Subject " + name
						+ " add to list subject for group");
			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				logger.info("Result set close");

				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");

			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}
		return subjects;

	}

	public void updateSubject(String name, String newName,
			String typeLectureRoom) throws DAOException, NamingException {
		String sql = "UPDATE subject set name = ?, type_lecture_room = ? where name = ?";
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			logger.info("Connection open");
			statement = connection.prepareStatement(sql);
			logger.info("Statement create");
			statement.setString(1, newName);
			statement.setString(2, typeLectureRoom);
			statement.setString(3, name);

			int rowsInsert = statement.executeUpdate();
			if (rowsInsert > 0) {
				logger.info("Subject " + newName + " update");

			}

		} catch (SQLException e) {
			logger.error("Error connecting to database", e);
			throw new DAOException("Error connecting to database", e);
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				logger.info("Statement close");

				if (connection != null) {
					connection.close();
				}
				logger.info("Connection close");
			}

			catch (SQLException e) {
				logger.error(
						"Result set or statement or connection don't close", e);
				throw new DAOException(
						"Result set or statement or connection don't close", e);

			}
		}

	}
}
