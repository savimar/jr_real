package tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.naming.NamingException;

import org.junit.Test;

import dao.CellScheduleDao;
import dao.DAOException;
import domain.CellSchedule;
import domain.DomainException;

public class MainTest {

	@Test
	public void testCellShcedule() throws DAOException, NamingException, DomainException {
		CellScheduleDao cell = new CellScheduleDao();
		List<CellSchedule> cells = cell.findAll();
		assertEquals(179, cells.size());

	}
	/*
	 * @Test public void testDaoBase() throws DAOException, NamingException {
	 * StudentDao group = new StudentDao(); long id =
	 * group.findIdforName("student", "Petrova Irina"); assertEquals(3, id); }
	 */

}
