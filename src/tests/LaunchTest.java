package tests;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import domain.Group;
import domain.Launch;
import domain.LectureRoom;

public class LaunchTest {

	List<Group> groups = new ArrayList<Group>();
	List<LectureRoom> lectureRooms = new ArrayList<LectureRoom>();
	List<LocalDateTime> dataTimes = new ArrayList<LocalDateTime>();

	@Test(expected = Exception.class)
	public void testNullListGroup() throws Exception {
		lectureRooms.add(new LectureRoom("A348", 58, ""));
		dataTimes.add(LocalDateTime.of(2015, Month.SEPTEMBER, 10, 9, 30));
		Launch launch = new Launch();
		launch.runSchedule(groups, dataTimes, lectureRooms);

	}

	@Test(expected = Exception.class)
	public void testNullListLectureRoom() throws Exception {
		groups.add(new Group("UT53", null, null));
		dataTimes.add(LocalDateTime.of(2015, Month.SEPTEMBER, 8, 9, 30));
		Launch launch = new Launch();
		launch.runSchedule(groups, dataTimes, lectureRooms);

	}

	@Test(expected = Exception.class)
	public void testNullListDateTimes() throws Exception {
		groups.add(new Group("RT25", null, null));
		lectureRooms.add(new LectureRoom("C149", 30, ""));
		;
		Launch launch = new Launch();
		launch.runSchedule(groups, dataTimes, lectureRooms);

	}

}
