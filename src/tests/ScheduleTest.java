package tests;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.junit.Before;
import org.junit.Test;

import dao.DAOException;
import domain.Group;
import domain.Launch;
import domain.LectureRoom;
import domain.Student;
import domain.Subject;
import domain.Teacher;

public class ScheduleTest {

	List<Group> groups = new ArrayList<Group>();
	List<LectureRoom> lectureRooms = new ArrayList<LectureRoom>();
	List<LocalDateTime> dataTimes = new ArrayList<LocalDateTime>();

	@Before
	public void setUp() throws Exception {
		List<Student> students1 = new ArrayList<Student>();
		List<Student> students2 = new ArrayList<Student>();
		List<Subject> subjects1 = new ArrayList<Subject>();
		List<Subject> subjects2 = new ArrayList<Subject>();
		List<Teacher> teachers1 = new ArrayList<Teacher>();
		List<Teacher> teachers2 = new ArrayList<Teacher>();
		List<Teacher> teachers3 = new ArrayList<Teacher>();
		List<Teacher> teachers4 = new ArrayList<Teacher>();
		students1.add(new Student("Morozova Alina", "AT1067", "5098"));
		students1.add(new Student("Ivanov Sergey", "AT1078", "5098"));
		students1.add(new Student("Petrova Irina", "AT1065", "5098"));
		students2.add(new Student("Sidorov Ivan", "IT1069", "5102"));
		students2.add(new Student("Kuznetsov Dmitriy", "IT1075", "5102"));
		students2.add(new Student("Denisov Anton", "IT1081", "5102"));

		Teacher t1 = new Teacher("Bobkov Denis Nikolaevich", 12, "AT");
		Teacher t2 = new Teacher("Kholmogorova Irina Alexeevna", 16, "IN");
		Teacher t3 = new Teacher("Baranov Igor Petrovich", 20, "IT");
		Teacher t4 = new Teacher("Ivanov Petr Pavlovich", 5, "HS");
		Teacher t5 = new Teacher("Ivanova Vera Ivanivna", 18, "PH");
		teachers1.add(t3);
		teachers1.add(t1);
		teachers2.add(t2);
		teachers2.add(t1);
		teachers3.add(t4);
		teachers4.add(t5);

		Subject sub1 = new Subject("Mathematics", teachers1, "");
		Subject sub2 = new Subject("Physics", teachers2, "PhysicsLaboratory");
		Subject sub3 = new Subject("History", teachers3, "");
		Subject sub4 = new Subject("Physical Education", teachers4, "Gym");
		subjects1.add(sub1);
		subjects1.add(sub2);
		subjects2.add(sub4);
		subjects2.add(sub3);
		subjects2.add(sub4);
		subjects2.add(sub1);

		Group group1 = new Group("AT10", students1, subjects1);
		Group group2 = new Group("IT10", students2, subjects2);
		groups.add(group1);
		groups.add(group2);

		lectureRooms.add(new LectureRoom("A109", 100, "Gym"));
		lectureRooms.add(new LectureRoom("B239", 45, ""));
		lectureRooms.add(new LectureRoom("C438", 30, "PhysicsLaboratory"));
		lectureRooms.add(new LectureRoom("A287", 60, "Cinema"));
		lectureRooms.add(new LectureRoom("B423", 15, ""));
		lectureRooms.add(new LectureRoom("C325", 30, ""));

		dataTimes.add (LocalDateTime.of(2015, Month.SEPTEMBER, 5, 9, 30));
		dataTimes.add( LocalDateTime.of(2015, Month.SEPTEMBER, 5, 11, 15));
		dataTimes.add(LocalDateTime.of(2015, Month.SEPTEMBER, 5, 14, 0));
		dataTimes.add(LocalDateTime.of(2015, Month.SEPTEMBER, 5, 15, 15));
		dataTimes.add(LocalDateTime.of(2015, Month.SEPTEMBER, 6, 9, 30));
		dataTimes.add(LocalDateTime.of(2015, Month.SEPTEMBER, 6, 11, 15));

	}

	@Test
	public void test() throws DAOException, NamingException {
		Launch launch = new Launch();
		launch.runSchedule(groups, dataTimes, lectureRooms);

	}

}
