package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;

import dao.DAOException;
import dao.StudentDao;

/**
 * student
 * 
 * @author B
 *
 */
@Entity
@Table(name = "student")
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(Student.class.getName());

	public Student() throws DomainException {
	}

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "kaugen", strategy = "increment")
	@GeneratedValue(generator = "kaugen")
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "id_student")
	private String idStudent;
	@Column(name = "specialization")
	private String specialization;
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Group group;

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public String getIdStudent() {
		return idStudent;
	}

	public void setIdStudent(String idStudent) {
		this.idStudent = idStudent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Student(String name, String idStudent, String specialization) throws DomainException {
		this.name = name;
		this.idStudent = idStudent;
		this.specialization = specialization;
	}

	public Student createStudent(String name, String idStudent, String specialization) throws DAOException, NamingException {
		logger.info("Creating new student: name " + name + "idStudent " + idStudent);
		StudentDao studentBase = new StudentDao();
		Student student = null;
		try {
			student = studentBase.createStudent(name, idStudent, specialization);
		} catch (DAOException e) {
			logger.error("Student " + name + "didn't create", e);
		}
		return student;
	}

	public Student findStudentByName(String name) throws DAOException, DomainException, NamingException {
		logger.info("Search student by name " + name);
		logger.info("Reading student from DataBase");
		Student student = null;
		StudentDao studentBase = new StudentDao();
		try {
			student = studentBase.findByName(name);

		} catch (DAOException e) {
			logger.error("Student " + name + " not found", e);
		}

		return student;

	}

	public void deleteStudent(String name, String idStudent) throws DAOException, NamingException {
		logger.info(" student: name " + name + "idStudent " + idStudent);
		StudentDao studentBase = new StudentDao();
		try {
			studentBase.deleteStudent(name, idStudent);
		} catch (DAOException e) {
			logger.error("Student " + name + "idStudent " + idStudent + "didn't delete", e);
		}

	}

	public List<Student> findStudentsGroup(String groupName) throws DAOException, DomainException, NamingException {
		List<Student> students = new ArrayList<Student>();
		StudentDao studentBase = new StudentDao();
		try {
			groupName += "%";
			students = studentBase.findStudentsGroup(groupName);
		} catch (DAOException e) {
			logger.error("Student's group's name " + groupName + "not found", e);
		}

		return students;

	}

	public void updateStudent(String name, String idStudent, String newName, String newIdStudent, String newSpecialization) throws DAOException, NamingException {

		StudentDao studentBase = new StudentDao();

		try {
			studentBase.updateStudent(name, idStudent, newName, newIdStudent, newSpecialization);
		} catch (DAOException e) {
			logger.error("Student " + name + "didn't update", e);
		}

	}

	public void print() {
		System.out.println("Print right");

	}
}
