package domain;

/**
 * class to start the program
 */

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import dao.DAOException;
import dao.GroupDao;
import dao.LectureRoomDao;

public class Launch {
	static Logger logger = Logger.getLogger(Launch.class);
	List<Group> groups = new ArrayList<Group>();
	List<LocalDateTime> dates = new ArrayList<LocalDateTime>();
	List<LectureRoom> lectureRooms = new ArrayList<LectureRoom>();

	public Launch(List<LocalDateTime> dates) {
		this.dates = dates;
	}

	public Launch() {

	}

	public void runSchedule(List<Group> groups, List<LocalDateTime> dates, List<LectureRoom> lectureRooms) throws DAOException, NamingException {

		if (groups.size() > 0 && dates.size() > 0 && lectureRooms.size() > 0) {
			Schedule schedule = new Schedule();
			try {
				schedule.fillSchedules(groups, dates, lectureRooms);
			} catch (DomainException e) {
				logger.error("Do not fill the list of student's groups, list of lecture rooms, or a list of date and time for lectures", e);

			}
		} else {

			System.out.println("Do not fill the list of student's groups, list of lecture rooms, or a list of date and time for lectures");
			throw new NullPointerException();
		}
	}

	public List<String> runListSchedule(List<LocalDateTime> dates) throws DAOException, DomainException, NamingException {
		List<String> schedulesStr = new ArrayList<String>();
		GroupDao groupBase = new GroupDao();
		groups = groupBase.findListGroup();

		LectureRoomDao lectureRoomBase = new LectureRoomDao();
		lectureRooms = lectureRoomBase.findLectureRooms();

		if (groups.size() > 0 && dates.size() > 0 && lectureRooms.size() > 0) {
			Schedule schedule = new Schedule();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.uuuu kk:mm");
			try {
				List<CellSchedule> cellSchedules = schedule.fillListSchedules(groups, dates, lectureRooms);
				for (CellSchedule cellSchedule : cellSchedules) {
					schedulesStr.add(cellSchedule.getDateTimeStart().format(formatter));
					schedulesStr.add(cellSchedule.getGroup().getName());
					schedulesStr.add(cellSchedule.getLectureRoom().getName());
					schedulesStr.add(cellSchedule.getSubject().getName());
					schedulesStr.add(cellSchedule.getTeacher().getName());
				}

			} catch (DomainException e) {
				logger.error("Do not fill the list of student's groups, list of lecture rooms, or a list of date and time for lectures", e);
				e.printStackTrace();
			}
		} else {

			System.out.println("Do not fill the list of student's groups, list of lecture rooms, or a list of date and time for lectures");
			throw new NullPointerException();
		}

		return schedulesStr;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<LocalDateTime> getDates() {
		return dates;
	}

	public void setDates(List<LocalDateTime> dates) {
		this.dates = dates;
	}

	public List<LectureRoom> getLectureRooms() {
		return lectureRooms;
	}

	public void setLectureRooms(List<LectureRoom> lectureRooms) {
		this.lectureRooms = lectureRooms;
	}
}
