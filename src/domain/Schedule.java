package domain;

/**
 * scheduling
 */

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import dao.CellScheduleDao;
import dao.DAOException;
import dao.SubjectDao;

public class Schedule {
	static Logger logger = Logger.getLogger(Schedule.class);
	private List<CellSchedule> cellSchedules = new ArrayList<CellSchedule>();

	public Schedule() {

	}

	public Schedule(List<CellSchedule> cellSchedules) {
		this.cellSchedules = cellSchedules;
	}

	public List<CellSchedule> getCellSchedules() {
		return cellSchedules;
	}

	public void setCellSchedules(List<CellSchedule> cellSchedules) {
		this.cellSchedules = cellSchedules;
	}

	/**
	 * scheduling
	 * 
	 * @param groups
	 * @param dateTimes
	 * @param lectureRooms
	 * @throws DAOException
	 * @throws NamingException
	 */

	public void fillSchedules(List<Group> groups, List<LocalDateTime> dateTimes, List<LectureRoom> lectureRooms) throws DomainException, DAOException, NamingException {
		for (int i = 0; i < dateTimes.size(); i++) {
			for (Group group : groups) {
				if (dateTimes.size() <= i) {
					break;
				}
				CellScheduleDao cellScheduleDao = new CellScheduleDao();
				if (cellScheduleDao.findCellSchedule(group, dateTimes.get(i)) == null) {
					SubjectDao subjectBase = new SubjectDao();
					List<Subject> subjects = null;
					/*
					 * subjectBase .findSubjectGroup(subjectBase.findIdforName(
					 * "group_student", group.getName()));
					 */
					for (Subject subj : subjects) {
						CellSchedule cellSchedule = new CellSchedule();
						cellSchedule.setGroup(group);
						cellSchedule.setSubject(subj);
						Teacher teacher = null;
						LectureRoom lectureRoom = null;
						cellSchedule.setDateTimeStart(dateTimes.get(i));
						teacher = determineTeacher(dateTimes, subj, cellSchedule, teacher);
						cellSchedule.setTeacher(teacher);
						lectureRoom = determineLectureRoom(dateTimes, lectureRooms, group, subj, cellSchedule, lectureRoom);
						cellSchedule.setLectureRoom(lectureRoom);
						boolean isIt = isDuplicated(dateTimes.get(i), group, subj, teacher);
						if (!isIt) {
							recordeCellToDataBase(dateTimes, group, subj, cellSchedule, teacher, lectureRoom, i);
						}
					}

				}
			}
		}
	}

	public List<CellSchedule> fillListSchedules(List<Group> groups, List<LocalDateTime> dateTimes, List<LectureRoom> lectureRooms) throws DomainException, DAOException, NamingException {
		for (int i = 0; i < dateTimes.size(); i++) {
			for (Group group : groups) {
				if (dateTimes.size() <= i) {
					break;
				}
				CellScheduleDao cellScheduleDao = new CellScheduleDao();
				if (cellScheduleDao.findCellSchedule(group, dateTimes.get(i)) == null) {
					SubjectDao subjectBase = new SubjectDao();
					List<Subject> subjects = null;
					/*
					 * subjectBase .findSubjectGroup(subjectBase.findIdforName(
					 * "group_student", group.getName()));
					 */
					for (Subject subj : subjects) {
						CellSchedule cellSchedule = new CellSchedule();
						cellSchedule.setGroup(group);
						cellSchedule.setSubject(subj);
						Teacher teacher = null;
						LectureRoom lectureRoom = null;
						cellSchedule.setDateTimeStart(dateTimes.get(i));
						teacher = determineTeacher(dateTimes, subj, cellSchedule, teacher);
						cellSchedule.setTeacher(teacher);
						lectureRoom = determineLectureRoom(dateTimes, lectureRooms, group, subj, cellSchedule, lectureRoom);
						cellSchedule.setLectureRoom(lectureRoom);
						boolean isIt = isDuplicated(dateTimes.get(i), group, subj, teacher);
						if (!isIt) {
							recordeCellToDataBase(dateTimes, group, subj, cellSchedule, teacher, lectureRoom, i);
						}
					}

				}
			}
		}
		return cellSchedules;

	}

	private LectureRoom determineLectureRoom(List<LocalDateTime> dateTimes, List<LectureRoom> lectureRooms, Group group, Subject subj, CellSchedule cellSchedule, LectureRoom lectureRoom) {
		try {
			lectureRoom = cellSchedule.findLectureRoom(lectureRooms, subj, group.getCountStudents(), dateTimes.get(0));
			cellSchedule.setLectureRoom(lectureRoom);
		} catch (Exception e) {
			logger.error("Didn't set lecture room", e);
		}
		return lectureRoom;
	}

	private Teacher determineTeacher(List<LocalDateTime> dateTimes, Subject subj, CellSchedule cellSchedule, Teacher teacher) {
		try {
			teacher = cellSchedule.selectTeacher(subj, dateTimes.get(0));
			cellSchedule.setTeacher(teacher);
		} catch (Exception e) {
			logger.error("Didn't set teacher", e);
		}
		return teacher;
	}

	public void recordeCellToDataBase(List<LocalDateTime> dateTimes, Group group, Subject subj, CellSchedule cellSchedule, Teacher teacher, LectureRoom lectureRoom, int i) throws DAOException, NamingException {
		cellSchedule.createCellSchedule(group, teacher, lectureRoom, subj, dateTimes.get(i));
		cellSchedules.add(cellSchedule);
		logger.info("Cell schedule date " + dateTimes.get(i) + " group " + group.getName() + " subject " + subj.getName() + " teacher " + teacher.getName() + " included in the database");

	}

	@SuppressWarnings("null")
	public boolean isDuplicated(LocalDateTime date, Group group, Subject subject, Teacher teacher) throws DAOException {
		boolean isIt = false;
		CellScheduleDao cellScheduleBase = new CellScheduleDao();

		try {
			CellSchedule lCellSchedule = cellScheduleBase.findCellSchedule(group, date);
			if (lCellSchedule != null) {
				isIt = true;
			} else if (lCellSchedule.getTeacher().getName().equals(teacher.getName())) {
				isIt = true;
			} else if (lCellSchedule.getSubject().getName().equals(subject.getName())) {
				isIt = true;
			}

		} catch (Exception e) {
			logger.error("List cellSchedule didn't find for date " + date, e);
		}

		return isIt;
	}
}