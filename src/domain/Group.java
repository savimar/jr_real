package domain;

/**
 * studend's group
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;

import dao.DAOException;
import dao.GroupDao;

@Entity
@Table(name = "group_student")
public class Group implements Serializable {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(Group.class);

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "kaugen", strategy = "increment")
	@GeneratedValue(generator = "kaugen")
	private long id;
	@Column(name = "name")
	private String name;
	@OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Student> students;
	@Column(name = "name")
	private int countStudents;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "tbl_subject_MM_group", joinColumns = @JoinColumn(name = "group_id"), inverseJoinColumns = @JoinColumn(name = "subject_id"))
	private List<Subject> subjects;

	public List<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCountStudents() {
		return countStudents;
	}

	public void setCountStudents(List<Student> students) {
		this.countStudents = students.size();
	}

	public Group(String name, List<Student> students, List<Subject> subjects) throws DomainException {

		this.name = name;
		this.students = students;
		this.countStudents = students.size();
		this.subjects = subjects;
	}

	public Group(String name) throws DomainException {
		this.name = name;
	}

	public Group() throws DomainException {

	}

	public Group createGroup(String name) throws DAOException, NamingException {
		Group group = null;
		GroupDao groupBase = new GroupDao();
		try {
			group = groupBase.createGroup(name);
			logger.info("Group " + name + " created");
		} catch (DAOException e) {
			logger.error("Group " + name + " didn't create", e);
		}
		return group;
	}

	public void deleteGroup(String name) throws DAOException, NamingException {
		GroupDao groupBase = new GroupDao();
		try {
			groupBase.deleteGroup(name);
			logger.info("Group " + name + " deteted");
		} catch (DAOException e) {
			logger.error("Group " + name + " didn't delete", e);
		}
	}

	public Group findByName(String name) throws DAOException, DomainException, NamingException {
		Group group = null;
		GroupDao groupBase = new GroupDao();
		try {
			group = groupBase.findByName(name);
			logger.info("Group " + name + " found");
		} catch (DAOException e) {
			logger.error("Group " + name + " didn't find", e);
		}
		return group;
	}

	public List<Group> findGroupList() throws DAOException, DomainException, NamingException {
		List<Group> groups = new ArrayList<Group>();
		GroupDao groupBase = new GroupDao();
		try {
			groups = groupBase.findListGroup();
			logger.info("List groups by  found");
		} catch (DAOException e) {
			logger.error("List groups didn't find", e);
		}
		return groups;
	}

	public void updateGroup(String name, String newName) throws DAOException, NamingException {
		GroupDao groupBase = new GroupDao();
		try {
			groupBase.updateGroup(name, newName);
			logger.info("Group " + name + " updated");
		} catch (DAOException e) {
			logger.error("Group " + name + " didn't update", e);
		}
	}
}