package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;

import dao.DAOException;
import dao.LectureRoomDao;

/**
 * lecture room
 * 
 * @author B
 *
 */
@Entity
@Table(name = "lecture_room")
public class LectureRoom implements Serializable {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(LectureRoom.class);
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "kaugen", strategy = "increment")
	@GeneratedValue(generator = "kaugen")
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "spaciousness")
	private int spaciousness;
	@Column(name = "type")
	private String type;

	public LectureRoom() throws DomainException {
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSpaciousness() {
		return spaciousness;
	}

	public void setSpaciousness(int spaciousness) {
		this.spaciousness = spaciousness;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LectureRoom(String name, int spaciousness, String type) throws DomainException {

		this.name = name;
		this.spaciousness = spaciousness;
		this.type = type;
	}

	public LectureRoom createLectureRoom1(String name, int spaciousness, String type) throws DAOException, NamingException {
		LectureRoom lectureRoom = null;
		LectureRoomDao lectureRoomBase = new LectureRoomDao();

		try {
			lectureRoom = lectureRoomBase.createLectureRoom(name, spaciousness, type);
			logger.info("Lecture room " + name + " created");
		} catch (DAOException e) {
			logger.error("Lecture room " + name + " didn't create", e);
		}
		return lectureRoom;
	}

	public void deleteLectureRoom1(String name) throws DAOException, NamingException {
		LectureRoomDao lectureRoomBase = new LectureRoomDao();
		try {
			lectureRoomBase.deleteLectureRoom(name);
			logger.info("LectureRoom " + name + " deteted");
		} catch (DAOException e) {
			logger.error("LectureRoom " + name + " didn't delete", e);
		}
	}

	public LectureRoom findByName(String name) throws DAOException, DomainException, NamingException {
		LectureRoom lectureRoom = null;
		LectureRoomDao lectureRoomBase = new LectureRoomDao();
		try {
			lectureRoom = lectureRoomBase.findByName(name);
			logger.info("Lecture room " + name + " found");
		} catch (DAOException e) {
			logger.error("Lecture room " + name + " didn't find", e);
		}
		return lectureRoom;

	}

	public List<LectureRoom> findLectureRoomsGroups() throws DAOException, DomainException, NamingException {
		List<LectureRoom> lectureRooms = new ArrayList<LectureRoom>();
		LectureRoomDao lectureRoomBase = new LectureRoomDao();
		try {
			lectureRooms = lectureRoomBase.findLectureRooms();
			logger.info("List lecture room found");
		} catch (DAOException e) {
			logger.error("List lecture room didn't find", e);
		}
		return lectureRooms;
	}

	public void updateLectureRoom(String name, String newName, int newSpaciousness, String newType) throws DAOException, NamingException {
		LectureRoomDao lectureRoomBase = new LectureRoomDao();
		try {
			lectureRoomBase.updateLectureRoom(name, newName, newSpaciousness, newType);
			logger.info("Lecture room " + name + " updated");
		} catch (DAOException e) {
			logger.error("Lecture room " + name + " didn't update", e);
		}

	}

}
