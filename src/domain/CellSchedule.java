package domain;

/**
 * table cell schedule
 */
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import dao.CellScheduleDao;
import dao.DAOException;
import dao.TeacherDao;

@Repository
@Scope("prototype")
@Entity
@Table(name = "cell_schedule")
public class CellSchedule extends Domain implements Serializable {

	static Logger logger = Logger.getLogger(CellSchedule.class);

	public static final long serialVersionUID = 170501995l;
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "kaugen", strategy = "increment")
	@GeneratedValue(generator = "kaugen")
	private long id;
	@ManyToOne
	@JoinColumn(name = "group_id")
	private Group group;
	@ManyToOne
	@JoinColumn(name = "teacher_id")
	private Teacher teacher;
	@ManyToOne
	@JoinColumn(name = "lecture_room_id")
	private LectureRoom lectureRoom;
	@ManyToOne
	@JoinColumn(name = "subject_id")
	private Subject subject;
	@Temporal(TemporalType.DATE)
	@Column(name = "data_time_start")
	private LocalDateTime dateTimeStart;

	public LocalDateTime getDateTimeStart() {
		return dateTimeStart;
	}

	public void setDateTimeStart(LocalDateTime dateTimeStart) {
		this.dateTimeStart = dateTimeStart;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public LectureRoom getLectureRoom() {
		return lectureRoom;
	}

	public void setLectureRoom(LectureRoom lectureRoom) {
		this.lectureRoom = lectureRoom;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public CellSchedule(Group group, Teacher teacher, LectureRoom lectureRoom, Subject subject, LocalDateTime dateTimeStart) throws DomainException {
		super();
		this.group = group;
		this.teacher = teacher;
		this.lectureRoom = lectureRoom;
		this.subject = subject;
		this.dateTimeStart = dateTimeStart;
	}

	/**
	 * choose teacher
	 * 
	 * @param teacher
	 * @param dateTimeStart
	 * @return
	 * @throws DAOException
	 * @throws NamingException
	 */
	public Teacher selectTeacher(Subject subject, LocalDateTime dateTimeStart) throws DomainException, DAOException, NamingException {
		TeacherDao teacherBase = new TeacherDao();
		// List<Teacher> teachers =
		// teacherBase.findTeachersGroup(teacherBase.findIdforName("subject",
		// subject.getName()));

		/*
		 * Teacher teacher = null; for (int i = 0; i < teachers.size(); i++) {
		 * if (teachers.get(i).getNumberHours() < Teacher.MAX_NUMBER_HOURS) {
		 * teacher = teachers.get(i); if (!teacherIsBusy(teacher,
		 * dateTimeStart)) { break; } } }
		 */

		return teacher;

	}

	/**
	 * select lecture room
	 * 
	 * @param lectureRooms
	 * @param subject
	 * @param countStudents
	 * @return
	 * @throws DAOException
	 * @throws NamingException
	 */

	public LectureRoom findLectureRoom(List<LectureRoom> lectureRooms, Subject subject, int countStudents, LocalDateTime dateTimeStart) throws DomainException, DAOException, NamingException {
		LectureRoom lectureRoom = null;
		for (int i = 0; i < lectureRooms.size(); i++) {
			if (subject.getTypeLectureRoom() == null && (lectureRooms.get(i).getSpaciousness() >= group.getCountStudents())) {
				lectureRoom = lectureRooms.get(i);
				lectureRooms.remove(i);
				if (!lectureRoomIsBusy(lectureRoom, dateTimeStart)) {
					break;
				}
			} else if (subject.getTypeLectureRoom().equals(lectureRooms.get(i).getType()) && (lectureRooms.get(i).getSpaciousness() >= countStudents)) {
				lectureRoom = lectureRooms.get(i);
				if (!lectureRoomIsBusy(lectureRoom, dateTimeStart)) {
					break;
				}
			}
		}
		return lectureRoom;

	}

	public CellSchedule() throws DomainException {

	}

	public CellSchedule createCellSchedule(Group group, Teacher teacher, LectureRoom lectureRoom, Subject subject, LocalDateTime dateTimeStart) throws DAOException, NamingException {

		CellSchedule cellSchedule = null;
		CellScheduleDao cellScheduleBase = new CellScheduleDao();
		try {
			cellSchedule = cellScheduleBase.createCellSchedule(group, teacher, lectureRoom, subject, dateTimeStart);
			logger.info("cell schedule for group" + group.getName() + " at " + dateTimeStart + " created");
		} catch (DAOException e) {
			logger.error("cell schedule for group" + group.getName() + " at " + dateTimeStart + " didn't create", e);
		}
		return cellSchedule;
	}

	/**
	 * is busy lecture room?
	 * 
	 * @param lectureRoom
	 * @param dateTimeStart
	 * @return
	 * @throws DAOException
	 * @throws DomainException
	 * @throws NamingException
	 */
	private boolean lectureRoomIsBusy(LectureRoom lectureRoom, LocalDateTime dateTimeStart) throws DAOException, DomainException, NamingException {
		boolean isBusy = false;
		CellScheduleDao cellScheduleBase = new CellScheduleDao();
		try {
			List<CellSchedule> cellSchedules = cellScheduleBase.findListCellScheduleByDate(dateTimeStart);
			if (cellSchedules.size() > 0) {
				logger.info("List cellSchedule by  found");
			}
			for (CellSchedule cellSchedule : cellSchedules) {
				if (cellSchedule.getLectureRoom().equals(lectureRoom)) {
					isBusy = true;
					break;
				}
			}

		} catch (DAOException e) {
			logger.error("List cellSchedule didn't find", e);

		}
		return isBusy;
	}

	private boolean teacherIsBusy(Teacher teacher, LocalDateTime dateTimeStart) throws DAOException, DomainException, NamingException {
		boolean isBusy = false;
		CellScheduleDao cellScheduleBase = new CellScheduleDao();
		List<CellSchedule> cellSchedules = new ArrayList<CellSchedule>();
		try {
			cellSchedules = cellScheduleBase.findListCellScheduleByDate(dateTimeStart);
			if (cellSchedules.size() > 0) {
				logger.info("List cellSchedule by  found");
			}
			for (CellSchedule cellSchedule : cellSchedules) {
				if (cellSchedule.getTeacher().equals(teacher)) {
					isBusy = true;
					break;
				}
			}

		} catch (DAOException e) {
			logger.error("List cellSchedule didn't find", e);

		}
		return isBusy;
	}
}
