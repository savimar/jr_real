package domain;

/**
 * subject in high school
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.apache.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;

import dao.DAOException;
import dao.SubjectDao;

@Entity
@Table(name = "subject")
public class Subject implements Serializable {
	private static final long serialVersionUID = 1L;

	static Logger logger = Logger.getLogger(Subject.class);
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "kaugen", strategy = "increment")
	@GeneratedValue(generator = "kaugen")
	private long id;
	@Column(name = "name")
	private String name;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "tbl_subject_MM_teacher", joinColumns = @JoinColumn(name = "subject_id"), inverseJoinColumns = @JoinColumn(name = "teacher_id"))
	private List<Teacher> teachers;
	@Column(name = "type_lecture_room")
	private String typeLectureRoom;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "tbl_subject_MM_group", joinColumns = @JoinColumn(name = "subject_id"), inverseJoinColumns = @JoinColumn(name = "group_id"))
	private List<Group> groups;

	public String getTypeLectureRoom() {
		return typeLectureRoom;
	}

	public void setTypeLectureRoom(String typeLectureRoom) {
		this.typeLectureRoom = typeLectureRoom;
	}

	public List<Teacher> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Subject(String name, List<Teacher> teachers, String typeLectureRoom) throws DomainException {
		this.name = name;
		this.teachers = teachers;
		this.typeLectureRoom = typeLectureRoom;
	}

	public Subject() throws DomainException {
	}

	public Subject(String name, String typeLectureRoom) throws DomainException {
		this.name = name;
		this.typeLectureRoom = typeLectureRoom;
	}

	public Subject createSubject(String name, String typeLectureRoom) throws DAOException, NamingException {

		Subject subject = null;
		SubjectDao subjectBase = new SubjectDao();
		try {
			subject = subjectBase.createSubject(name, typeLectureRoom);
			logger.info("Subject " + name + " created");
		} catch (DAOException e) {
			logger.error("Subject " + name + " didn't create", e);
		}
		return subject;
	}

	public void deleteSubject(String name) throws DAOException, NamingException {
		SubjectDao subjectBase = new SubjectDao();
		try {
			subjectBase.deleteSubject(name);
			logger.info("Subject " + name + " deteted");
		} catch (DAOException e) {
			logger.error("Subject " + name + " didn't delete", e);
		}
	}

	public Subject findByName(String name) throws DAOException, DomainException, NamingException {
		Subject subject = null;
		SubjectDao subjectBase = new SubjectDao();
		try {
			subject = subjectBase.findByName(name);
			logger.info("Subject " + name + " found");
		} catch (DAOException e) {
			logger.error("Subject " + name + " didn't find", e);
		}
		return subject;
	}

	public List<Subject> findGroupSubgects(int idGroup) throws DAOException, DomainException, NamingException {
		List<Subject> subjects = new ArrayList<Subject>();
		SubjectDao subjectBase = new SubjectDao();
		try {
			subjects = subjectBase.findSubjectGroup(idGroup);
			logger.info("List teachers by " + idGroup + " found");
		} catch (DAOException e) {
			logger.error("ID subject " + idGroup + " didn't find", e);
		}
		return subjects;
	}

	public void updateSublect(String name, String newName, String typeLectureRoom) throws DAOException, NamingException {
		SubjectDao subjectBase = new SubjectDao();
		try {
			subjectBase.updateSubject(name, newName, typeLectureRoom);
			logger.info("Subject " + name + " updated");
		} catch (DAOException e) {
			logger.error("Subject " + name + " didn't update", e);
		}

	}
}
