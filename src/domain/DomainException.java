package domain;

public class DomainException extends Exception {

	/**
	 * Domain Exception
	 */
	private static final long serialVersionUID = 2569L;

	public DomainException() {
		super();
	}

	public DomainException(String message) {
		super(message);

	}

	public DomainException(String message, Throwable cause) {
		super(message, cause);

	}

}
