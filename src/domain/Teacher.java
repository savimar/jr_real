package domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.apache.log4j.Logger;
import org.hibernate.annotations.GenericGenerator;

import dao.DAOException;
import dao.TeacherDao;

/**
 * teacher
 * 
 * @author B
 *
 */
@Entity
@Table(name = "teacher")
public class Teacher implements Serializable {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(Teacher.class);

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "kaugen", strategy = "increment")
	@GeneratedValue(generator = "kaugen")
	private long id;
	@Column(name = "name")
	private String name;
	@Column(name = "number_hours")
	private int numberHours;
	@Column(name = "chair")
	private String chair;
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "tbl_subject_MM_teacher", joinColumns = @JoinColumn(name = "teacher_id"), inverseJoinColumns = @JoinColumn(name = "subject_id"))
	private List<Subject> subjects;

	public static final int MAX_NUMBER_HOURS = 20;

	public String getChair() {
		return chair;
	}

	public void setChair(String chair) {
		this.chair = chair;
	}

	public int getNumberHours() {
		return numberHours;
	}

	public void setNumberHours(int numberHours) {
		this.numberHours = numberHours;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Teacher(String name, int numberHours, String chair) throws DomainException {

		this.name = name;
		this.numberHours = numberHours;
		this.chair = chair;
	}

	public Teacher() throws DomainException {
	}

	public Teacher findByName(String name) throws DAOException, DomainException, NamingException {
		Teacher teacher = null;
		TeacherDao teacherBase = new TeacherDao();
		try {
			teacher = teacherBase.findByName(name);
			logger.info("Teacher " + name + " found");
		} catch (DAOException e) {
			logger.error("Teacher " + name + " didn't find", e);
		}
		return teacher;

	}

	public Teacher createTeacher(String name, int numberHours, String chair) throws DAOException, NamingException {
		Teacher teacher = null;
		TeacherDao teacherBase = new TeacherDao();
		try {
			teacher = teacherBase.createTeacher(name, numberHours, chair);
			logger.info("Teacher " + name + " created");
		} catch (DAOException e) {
			logger.error("Teacher " + name + " didn't create", e);
		}
		return teacher;

	}

	public void deleteTeacher(String name, String chair) throws DAOException, NamingException {
		TeacherDao teacherBase = new TeacherDao();
		try {
			teacherBase.deleteTeacher(name, chair);
			logger.info("Teacher " + name + " deteted");
		} catch (DAOException e) {
			logger.error("Teacher " + name + " didn't delete", e);
		}
	}

	public List<Teacher> findTeacherSroupBySubject(int idSubject) throws DAOException, DomainException, NamingException {
		List<Teacher> teachers = new ArrayList<Teacher>();
		TeacherDao teacherBase = new TeacherDao();
		try {
			teachers = teacherBase.findTeachersGroup(idSubject);
			logger.info("List teachers by " + idSubject + " found");
		} catch (DAOException e) {
			logger.error("ID subject " + idSubject + " didn't find", e);
		}
		return teachers;

	}

	public void updateTeacher(String name, String chair, String newName, int newNumberHours, String newChair) throws DAOException, NamingException {

		TeacherDao teacherBase = new TeacherDao();
		try {
			teacherBase.updateTeacher(name, chair, newName, newChair, newNumberHours);
			logger.info("Teacher " + name + " updated");
		} catch (DAOException e) {
			logger.error("Teacher " + name + " didn't update", e);
		}

	}
}
