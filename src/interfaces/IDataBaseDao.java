package interfaces;

import java.util.List;

import domain.Domain;

public interface IDataBaseDao {
	List<Domain> findAll();

	List<Domain> findAllWithDetail();

	Domain findByid(Long id);

	Domain findByName(String name);

	Domain create(Domain dao);

	void delete(Domain dao);
}
