package interfaces;

import java.util.List;

import domain.CellSchedule;

public interface ICellScheduleDao {
	List<CellSchedule> findAll();

	/*
	 * List<CellSchedule> findListByDate(LocalDateTime dateTime);
	 * 
	 * 
	 * CellSchedule findById(Long id);
	 * 
	 * CellSchedule findBtGrooupAndDate(Group group, LocalDateTime dateTime);
	 * 
	 * CellSchedule findByName(String name);
	 */

	CellSchedule create(CellSchedule cellSchedule);

	void delete(CellSchedule dao);

	void update(CellSchedule cellSchedule, CellSchedule newCellSchedule);

}
