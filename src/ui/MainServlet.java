package ui;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.DAOException;
import domain.DomainException;
import domain.Launch;

//import org.apache.log4j.Logger;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(MainServlet.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainServlet() {
		super();
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dateStrings = request.getParameter("datetime-local");
		dateStrings = replaceString(dateStrings);
		String[] datesArray = dateStrings.split(";");
		List<LocalDateTime> dataTimes = new ArrayList<LocalDateTime>();
		try {
			for (String date : datesArray) {
				dataTimes.add(LocalDateTime.parse(date));
			}
			logger.info("The entered date is correct");
		} catch (Exception e) {
			logger.error("User entered the wrong format for the date for the schedule", e);
			request.setAttribute("message", "You entered the wrong format for the date for the schedule");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		// Launch launch = new Launch();
		ApplicationContext context = new ClassPathXmlApplicationContext("resourses/applicationContext.xml");
		Launch launch = (Launch) context.getBean("launch");
		try {
			List<String> schedules = launch.runListSchedule(dataTimes);
			if (schedules.size() == 0) {
				logger.error("The schedule is already created on this date");
				request.setAttribute("message", "The schedule is already created on this date");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			} else {
				logger.info("List schedule is created");
				request.setAttribute("table", schedules);
			}
			request.getRequestDispatcher("pages/schedule.jsp").forward(request, response);
		} catch (DAOException e) {
			logger.error("It is not possible to create a list to schedule. Error in the DAO layer", e);
			request.setAttribute("message", "It is not possible to create a list to schedule. Error in the DAO layer");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		} catch (DomainException e) {
			logger.error("It is not possible to create a list to schedule. Error in the domain layer", e);
			request.setAttribute("message", "It is not possible to create a list to schedule. Error in the domain layer");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		} catch (NamingException e) {
			logger.error("It is not possible to create a list to schedule. NamingException", e);
			request.setAttribute("message", "It is not possible to create a list to schedule. NamingException");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

	private String replaceString(String dateStrings) {
		dateStrings = dateStrings.replaceAll("\r\n", "");
		dateStrings = dateStrings.replaceAll(" ", "");
		return dateStrings;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
